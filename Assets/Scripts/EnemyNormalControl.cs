using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyNormalControl : Enemy
{

    private Vector2 originPosition;

    private enum SwitchMachineStates { NONE, IDLE, WALK, ATTACK, RECIVEHIT, DIE };
    private SwitchMachineStates currentState;
    private SwitchMachineStates lastState;
    private bool flip_h = false;

    private void Awake()
    {
        patrol = false;
    }

    void Start()
    {
        animator = GetComponent<Animator>();
        statsEnemy = LevelManager.Instance.GetRNGNormalEnemy();
        animator.runtimeAnimatorController = statsEnemy.animator;
        hpMax = statsEnemy.maxHP;
        hpActual = hpMax;
        moveSpeed = statsEnemy.moveSpeed;
        damage = statsEnemy.damage;
        rangeDetection = statsEnemy.rangeDetection;
        rangeAttack = statsEnemy.rangeAttack;
        GetComponent<SpriteRenderer>().color = statsEnemy.color;

        agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        agent.autoBraking = false;
        agent.updateRotation = false;
        agent.updateUpAxis = false;

        player = GameManager.Instance.GetPlayerController();

        originPosition = this.transform.position;
        InitState(SwitchMachineStates.IDLE);
    }

    void Update()
    {

        playerRaycast = Physics2D.OverlapCircle(transform.position, rangeDetection, layerMask);

        UpdateState();


        if (!inmune && playerRaycast != null)
            TrackedPlayer();

        float distance = Vector2.Distance(originPosition, this.transform.position);
        if (playerRaycast == null && playerTraked)
            StartCoroutine(StayOnSite());
        else if (distance > 0.4f && playerRaycast == null)
            agent.SetDestination(originPosition);
        else if(distance < 0.4f && playerRaycast == null)
            agent.speed = 0;

    }

    IEnumerator StayOnSite()
    {
        agent.speed = 0;
        yield return new WaitForSeconds(1.5f);
        agent.speed = moveSpeed;
        playerTraked = false;
    }

    void TrackedPlayer()
    {
        float distance = Vector2.Distance(player.transform.position, transform.position);
        if (distance < rangeAttack)
        {
            agent.speed = 0;
            if (canAttack)
            {
                ShootToPlayer();
                ChangeState(SwitchMachineStates.ATTACK);
            }
        }
        else
        {
            agent.speed = moveSpeed;
            agent.SetDestination(player.transform.position);
        }
        playerTraked = true;

    }

    private void ShootToPlayer()
    {
        canAttack = false;
        gameObject.transform.GetChild(0).GetComponent<HitboxInfo>().hitDamage = damage;
        StartCoroutine(ShootCoolDown(1.5f));
    }

    IEnumerator ShootCoolDown(float time)
    {
        yield return new WaitForSeconds(time);
        canAttack = true;
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (!inmune || collision.gameObject.GetComponentInParent<PlayerController>().comboDone)
        {
            collision.gameObject.GetComponentInParent<PlayerController>().comboDone = false;
            inmune = true;
            hpActual -= collision.gameObject.GetComponent<HitboxInfo>().hitDamage;
            if (hpActual <= 0)
                ChangeState(SwitchMachineStates.DIE);
            else
                ChangeState(SwitchMachineStates.RECIVEHIT);
        }

    }

    #region SwitchMachineStates

    private void ChangeState(SwitchMachineStates newState)
    {
        if (newState == currentState)
            return;

        ExitState();
        InitState(newState);
    }

    private void InitState(SwitchMachineStates currentState)
    {
        this.currentState = currentState;
        switch (this.currentState)
        {
            case SwitchMachineStates.IDLE:

                agent.speed = 0;
                    animator.Play("IdleDown");

                break;


            case SwitchMachineStates.WALK:
                if (flip_h)
                    transform.Rotate(0, 180, 0);
                else
                    transform.Rotate(0, -180, 0);
                animator.Play("Walk");

                break;


            case SwitchMachineStates.ATTACK:
                agent.speed = 0;
                animator.Play("Attack");

                break;

            case SwitchMachineStates.RECIVEHIT:
                agent.speed = 0;
                GetComponent<SpriteRenderer>().color = Color.red;
                StartCoroutine(StunTime());
                break;

            case SwitchMachineStates.DIE:
                LevelManager.Instance.OnEnemyDie();
                Destroy(gameObject);
                break;

            default:
                break;
        }
    }
    IEnumerator StunTime()
    {
        yield return new WaitForSeconds(1f);
        ChangeState(lastState);
    }

    private void ExitState()
    {
        switch (currentState)
        {
            case SwitchMachineStates.IDLE:

                break;

            case SwitchMachineStates.WALK:

                break;

            case SwitchMachineStates.ATTACK:
                ShootToPlayer();
                break;

            case SwitchMachineStates.RECIVEHIT:
                inmune = false;
                GetComponent<SpriteRenderer>().color = statsEnemy.color;
                break;

            default:
                break;
        }
    }

    private void UpdateState()
    {
        Vector3 velocityNormalized = agent.velocity.normalized;
        switch (currentState)
        {
            case SwitchMachineStates.IDLE:

                if (agent.speed != 0)
                    ChangeState(SwitchMachineStates.WALK);

                lastState = currentState;
                break;

            case SwitchMachineStates.WALK:

                if (agent.speed == 0)
                    ChangeState(SwitchMachineStates.IDLE);
                if (Mathf.Abs(velocityNormalized.y) < Mathf.Abs(velocityNormalized.x))
                {
                    if (velocityNormalized.x < 0)
                        flip_h = false;
                    if (velocityNormalized.x > 0)
                        flip_h = true;
                    ChangeState(SwitchMachineStates.WALK);
                }

                lastState = currentState;
                break;

            case SwitchMachineStates.ATTACK:
                break;
            case SwitchMachineStates.RECIVEHIT:
                break;
            case SwitchMachineStates.DIE:
                break;

            default:
                break;
        }
    }

    private void EndAnimation()
    {
        ChangeState(lastState);
    }
    #endregion

}
