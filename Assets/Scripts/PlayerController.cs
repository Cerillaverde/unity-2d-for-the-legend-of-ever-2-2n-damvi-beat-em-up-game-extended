using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.InputSystem;
using static UnityEditorInternal.VersionControl.ListControl;
using static UnityEngine.RuleTile.TilingRuleOutput;

public class PlayerController : MonoBehaviour
{
    private PlayerInput playerInput;
    private Vector2 movement;
    private Animator animator;
    private Rigidbody2D playerRigidbody;

    [SerializeField] private HitboxInfo hitboxInfo;

    [Space(10)]
    [Header("Stats of player")]
    [SerializeField] private SOPlayer playerSO;
    private float speed;
    private int hpMax;
    private int hpActual;
    private int attack1Damage;
    private int attack2Damage;
    private int comboDamage;


    [Header("Memory variables")]
    private bool flip_h;
    private bool flip_v;
    private bool inmune;
    [HideInInspector] public bool comboDone;
    [HideInInspector] public bool comboUp;

    private enum SwitchMachineStates { NONE, IDLEV, IDLEH, WALKV, WALKH, ATTACK1, ATTACK2, RECIVEHIT, DIE};
    private SwitchMachineStates currentState; 
    private SwitchMachineStates lastState;


    void Awake()
    {
        playerInput = GetComponent<PlayerInput>();
        animator = GetComponent<Animator>();
        playerRigidbody = GetComponent<Rigidbody2D>();
    }

    private void Start()
    {
        speed = playerSO.speed;
        hpMax = playerSO.hpMax;
        hpActual = hpMax;
        attack1Damage = playerSO.attack1Damage;
        attack2Damage = playerSO.attack2Damage;
        comboDamage = playerSO.comboDamage;

        //set memory variables 
        movement = Vector2.zero;
        flip_h = false;
        flip_v = false;
        comboDone = false;

        GameManager.Instance.SetMaxHealtBar(hpMax);

        GameManager.Instance.SetPlayer(this);
        InitState(SwitchMachineStates.IDLEV);
    }

    void Update()
    {
        UpdateState();
        movement = playerInput.actions["Move"].ReadValue<Vector2>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!inmune && collision.CompareTag("Bullet"))
        {
            hpActual -= collision.gameObject.GetComponent<HitboxInfo>().hitDamage;
            if (!GameManager.Instance.audioSFXManager.GetComponent<AudioSource>().isPlaying)
                GameManager.Instance.audioSFXManager.playerHitAudioPlay();

            GameManager.Instance.SetHealtBar(hpActual);

            if (hpActual <= 0)
            {
                GameManager.Instance.LoadGameOverScene();
            }
            else
                ChangeState(SwitchMachineStates.RECIVEHIT);
        }

    }
     public void ResetHP()
    {
        hpActual = hpMax;
        GameManager.Instance.SetHealtBar(hpActual);
    }

    #region SwitchMachineStates

    private void ChangeState(SwitchMachineStates newState)
    {
        if (newState == currentState)
            return;

        ExitState();
        InitState(newState);
    }

    private void InitState(SwitchMachineStates currentState)
    {
        this.currentState = currentState;
        switch (this.currentState)
        {
            case SwitchMachineStates.IDLEV:

                playerRigidbody.velocity = Vector2.zero;
                if (!flip_v)
                    animator.Play("IdleDown");
                else
                    animator.Play("IdleTop");
                break;

            case SwitchMachineStates.IDLEH:

                playerRigidbody.velocity = Vector2.zero;
                animator.Play("IdleSide");
                break;

            case SwitchMachineStates.WALKV:

                if (movement.y < 0)
                {
                    animator.Play("WalkDown");
                    flip_v = false;
                }
                else if (movement.y > 0)
                {
                    animator.Play("WalkTop");
                    flip_v = true;
                }

                break;

            case SwitchMachineStates.WALKH:

                animator.Play("WalkSide");
                if (movement.x > 0 && !flip_h)
                {
                    transform.Rotate(0, 180, 0);
                    flip_h = true;
                }else if(movement.x < 0 && flip_h)
                {
                    transform.Rotate(0, 180, 0);
                    flip_h = false;
                }

                break;

            case SwitchMachineStates.ATTACK1:
                playerRigidbody.velocity = Vector2.zero;
                GameManager.Instance.audioSFXManager.enemyHitAudioPlay();
                switch (lastState)
                {
                    case SwitchMachineStates.IDLEH:
                    case SwitchMachineStates.WALKH:
                        animator.Play("AttackSide");

                        break;

                    case SwitchMachineStates.IDLEV:
                    case SwitchMachineStates.WALKV:
                        if (flip_v)
                            animator.Play("AttackTop");
                        else 
                            animator.Play("AttackDown");

                        break;


                }
                break;

            case SwitchMachineStates.ATTACK2:
                playerRigidbody.velocity = Vector2.zero;
                GameManager.Instance.audioSFXManager.enemyHitAudioPlay();
                switch (lastState)
                {
                    case SwitchMachineStates.IDLEH:
                    case SwitchMachineStates.WALKH:
                        animator.Play("Attack2Side");

                        break;

                    case SwitchMachineStates.IDLEV:
                    case SwitchMachineStates.WALKV:
                        if (flip_v)
                            animator.Play("Attack2Top");
                        else
                            animator.Play("Attack2Down");

                        break;


                }
                break;

            case SwitchMachineStates.RECIVEHIT:
                playerRigidbody.velocity = Vector2.zero;
                inmune = true;
                switch (lastState)
                {
                    case SwitchMachineStates.IDLEH:
                    case SwitchMachineStates.WALKH:
                        animator.Play("HitSide");

                        break;

                    case SwitchMachineStates.IDLEV:
                    case SwitchMachineStates.WALKV:
                        if (flip_v)
                            animator.Play("HitTop");
                        else
                            animator.Play("HitDown");

                        break;
                }

                break;
            case SwitchMachineStates.DIE:

                Destroy(gameObject);

                break;

            default:
                break;
        }
    }

    private void ExitState()
    {
        switch (currentState)
        {
            case SwitchMachineStates.IDLEV:

                break;

            case SwitchMachineStates.WALKV:

                break;

            case SwitchMachineStates.IDLEH:

                break;

            case SwitchMachineStates.WALKH:

                break;

            case SwitchMachineStates.ATTACK1:
                break;

            case SwitchMachineStates.ATTACK2:
                break;

            case SwitchMachineStates.RECIVEHIT:
                inmune = false;
                break;

            default:
                break;
        }
    }

    private void UpdateState()
    {
        switch (currentState)
        {
            case SwitchMachineStates.IDLEV:
            case SwitchMachineStates.IDLEH:

                if(movement.y != 0)
                    ChangeState(SwitchMachineStates.WALKV);
                else if(movement.x != 0)
                    ChangeState(SwitchMachineStates.WALKH);

                lastState = currentState;
                break;

            case SwitchMachineStates.WALKV:

                playerRigidbody.velocity = movement * speed;

                if (playerRigidbody.velocity == Vector2.zero)
                    ChangeState(SwitchMachineStates.IDLEV);
                if (movement.y == 0 && movement.x != 0)
                    ChangeState(SwitchMachineStates.WALKH);

                lastState = currentState;
                break;


            case SwitchMachineStates.WALKH:
                playerRigidbody.velocity = movement * speed;
                if (movement.y != 0)
                    ChangeState(SwitchMachineStates.WALKV);
                if (playerRigidbody.velocity == Vector2.zero)
                    ChangeState(SwitchMachineStates.IDLEH);

                lastState = currentState;
                break;

            case SwitchMachineStates.ATTACK1:
                break;

            case SwitchMachineStates.ATTACK2:
                break;

            default:
                break;
        }
    }

    public void Attack1Action()
    {
        switch (currentState)
        {
            case SwitchMachineStates.ATTACK2:
                if (comboUp)
                {
                    hitboxInfo.hitDamage = attack1Damage + comboDamage;
                    comboDone = true;
                    ChangeState(SwitchMachineStates.ATTACK1);
                }
                break;

            default:
                hitboxInfo.hitDamage = 1;
                ChangeState(SwitchMachineStates.ATTACK1);

                break;

        }
    }

    public void Attack2Action()
    {
        switch (currentState)
        {
            case SwitchMachineStates.ATTACK1:
                if (comboUp)
                {
                    hitboxInfo.hitDamage = attack2Damage + comboDamage;
                    comboDone = true;
                    ChangeState(SwitchMachineStates.ATTACK2);
                }
                break;

            default:
                hitboxInfo.hitDamage = 2;
                ChangeState(SwitchMachineStates.ATTACK2);

                break;

        }
    }

    public void EndAnimation()
    {
        comboDone = false;
        ChangeState(lastState);
    }
    #endregion


    private void OnDestroy()
    {
        GameManager.Instance.DestoyPlayer();
    }
}
